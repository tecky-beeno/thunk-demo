import { RootState } from './state'
export function setMemoAction(memo: string) {
  return {
    type: 'setMemo' as const,
    memo,
  }
}

export function setErrorAction(error: any) {
  return {
    type: 'setError' as const,
    error: error.toString(),
  }
}

export function setDownloadStatus(status: RootState['downloadStatus']) {
  return {
    type: 'setDownloadStatus' as const,
    status,
  }
}

export function setUploadStatus(status: RootState['uploadStatus']) {
  return {
    type: 'setUploadStatus' as const,
    status,
  }
}

export type RootAction =
  | ReturnType<typeof setMemoAction>
  | ReturnType<typeof setErrorAction>
  | ReturnType<typeof setUploadStatus>
  | ReturnType<typeof setDownloadStatus>
