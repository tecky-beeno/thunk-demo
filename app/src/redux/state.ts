export type RootState = {
  memo?: string
  error?: string
  downloadStatus: 'idle' | 'downloading' | 'success' | 'fail'
  uploadStatus: 'idle' | 'uploading' | 'success' | 'fail'
}
