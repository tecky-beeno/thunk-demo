import { ThunkDispatch } from 'redux-thunk'
import { RootState } from './state'
import { setDownloadStatus, setUploadStatus } from './action'
import { RootAction, setErrorAction, setMemoAction } from './action'
const { REACT_APP_API_ORIGIN } = process.env

export type RootThunkDispatch = ThunkDispatch<RootState, null, RootAction>

export function downloadMemoThunk() {
  return (dispatch: RootThunkDispatch) => {
    dispatch(setDownloadStatus('downloading'))
    fetch(REACT_APP_API_ORIGIN + '/memo')
      .then(res => res.json())
      .then(json => {
        if (json.error) {
          dispatch(setErrorAction(json.error))
          dispatch(setDownloadStatus('fail'))
        } else {
          dispatch(setMemoAction(json.data.memo))
          dispatch(setDownloadStatus('success'))
        }
      })
      .catch(error => {
        dispatch(setErrorAction(error))
        dispatch(setDownloadStatus('fail'))
      })
  }
}

export function uploadMemoThunk() {
  return (dispatch: RootThunkDispatch, getState: () => RootState) => {
    dispatch(setUploadStatus('uploading'))
    const memo = getState().memo
    fetch(REACT_APP_API_ORIGIN + '/memo', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({ memo }),
    })
      .then(res => res.json())
      .then(json => {
        if (json.error) {
          dispatch(setErrorAction(json.error))
          dispatch(setUploadStatus('fail'))
        } else {
          dispatch(setUploadStatus('success'))
        }
      })
      .catch(error => {
        dispatch(setErrorAction(error))
        dispatch(setUploadStatus('fail'))
      })
  }
}
