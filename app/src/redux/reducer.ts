import { RootState } from './state'
import { RootAction } from './action'

let initialState: RootState = {
  downloadStatus: 'idle',
  uploadStatus: 'idle',
}

export let rootReducer = (
  state: RootState = initialState,
  action: RootAction,
): RootState => {
  console.log(action)
  switch (action.type) {
    case 'setMemo':
      return {
        ...state,
        memo: action.memo,
      }
    case 'setError':
      return {
        ...state,
        memo: state.memo,
        error: action.error,
      }
    case 'setUploadStatus':
      return {
        ...state,
        uploadStatus: action.status,
        error: action.status === 'success' ? undefined : state.error,
      }
    case 'setDownloadStatus':
      return {
        ...state,
        downloadStatus: action.status,
        error: action.status === 'success' ? undefined : state.error,
      }
    default:
      return state
  }
}
