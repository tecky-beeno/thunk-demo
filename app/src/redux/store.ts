import { rootReducer } from './reducer'
import { applyMiddleware, compose, createStore } from 'redux'
import thunk from 'redux-thunk'

let rootEnhancer = compose(applyMiddleware(thunk))

export let store = createStore(rootReducer, rootEnhancer)
