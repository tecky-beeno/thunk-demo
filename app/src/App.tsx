import React from 'react'
import { useDispatch, useSelector } from 'react-redux'
import './App.css'
import { setMemoAction } from './redux/action'
import { RootState } from './redux/state'
import { downloadMemoThunk, uploadMemoThunk } from './redux/thunk'

function App() {
  const dispatch = useDispatch()
  const error = useSelector((state: RootState) => state.error)
  const memo = useSelector((state: RootState) => state.memo)
  const uploadStatus = useSelector((state: RootState) => state.uploadStatus)
  const downloadStatus = useSelector((state: RootState) => state.downloadStatus)
  function setMemo(memo: string) {
    dispatch(setMemoAction(memo))
  }
  function download() {
    dispatch(downloadMemoThunk())
  }
  function upload() {
    dispatch(uploadMemoThunk())
  }
  return (
    <div className="App">
      <h1>memo</h1>
      <textarea value={memo} onChange={e => setMemo(e.target.value)}></textarea>
      <div>
        <button onClick={download} disabled={downloadStatus === 'downloading'}>
          download
        </button>
        <button onClick={upload} disabled={uploadStatus === 'uploading'}>
          {uploadStatus === 'uploading' ? 'uploading...' : 'upload'}
        </button>
      </div>
      <p className="error">{error}</p>
      <pre>
        <code>{JSON.stringify({ uploadStatus, downloadStatus }, null, 2)}</code>
      </pre>
    </div>
  )
}

export default App
