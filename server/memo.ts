import express from 'express'

let router = express.Router()

export default router

let memo = 'empty'

router.get('/memo', (req, res) => {
  res.json({ data: { memo } })
})

router.post('/memo', (req, res) => {
  if (!req.body.memo) {
    res.status(400).json({ error: 'memo cannot be empty' })
    return
  }
  memo = req.body.memo
  res.json({ data: 'ok' })
})
